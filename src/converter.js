const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex );
}

module.exports = {
    rgbToHex: (red, green, blue) => {
        const redHex = red.toString(16);
        const greenHex = green.toString(16);
        const blueHex = blue.toString(16);

        return pad(redHex) + pad(greenHex) + pad(blueHex);
    },
    hexToRGB: (hex) => {
        const hexInt = parseInt(hex, 16);
        const redRGB = (hexInt >> 16) & 255;
        const greenRGB = (hexInt >> 8) & 255;
        const blueRGB = hexInt & 255;

        return redRGB.toString() + ", " + greenRGB.toString() + ", " + blueRGB.toString();
    }
}